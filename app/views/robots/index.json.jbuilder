json.array!(@robots) do |robot|
  json.extract! robot, :id, :name, :weapon, :weight
  json.url robot_url(robot, format: :json)
end
