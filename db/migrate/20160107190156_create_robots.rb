class CreateRobots < ActiveRecord::Migration
  def change
    create_table :robots do |t|
      t.string :name
      t.string :weapon
      t.integer :weight

      t.timestamps null: false
    end
  end
end
