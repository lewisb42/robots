class AddWeightClassToRobots < ActiveRecord::Migration
  def change
    add_reference :robots, :weight_class, index: true, foreign_key: true
  end
end
