class RemoveWeightFromRobots < ActiveRecord::Migration
  def change
    remove_column :robots, :weight, :integer
  end
end
