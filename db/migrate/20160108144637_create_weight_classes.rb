class CreateWeightClasses < ActiveRecord::Migration
  def change
    create_table :weight_classes do |t|
      t.string :name
      t.integer :pounds

      t.timestamps null: false
    end
  end
end
