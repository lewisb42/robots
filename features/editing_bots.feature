Feature: Editing Bots

    Scenario: Changing a bot's weight class
        Given Bot "Uberclocker" is in the "Featherweight" weight class
        And Alice is on the Edit page for "Uberclocker"
        When Alice changes "Uberclocker"'s weight class to "Sportsman"
        Then Alice is directed to the Show page for "Uberclocker"
        And "Uberclocker" is now listed as the "Sportsman" weight class
	
    Scenario: Changing a bot's name
        Given Bob is on "Uberclocker"'s edit page
        When Bob changes the name to "Uberclocker Advance"
        Then Bob is directed to the Show page for "Uberclocker Advance"