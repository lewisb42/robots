Given(/^Bob is on "(.*?)"'s edit page$/) do |botname|
  wc = WeightClass.new(name: "Sportsman", pounds: 30)
  wc.save!
  mybot = Robot.new(name: botname, weight_class: wc, weapon: 'lifter')
  mybot.save!
  id = mybot.id
  visit "/robots/#{id}/edit"
  
end

When(/^Bob changes the name to "(.*?)"$/) do |newbotname|
  fill_in('Name', :with => newbotname)
  click_on('Update Robot')
end

Then(/^Bob is directed to the Show page for "(.*?)"$/) do |botname|
	id = Robot.find_by(name: botname).id
	path = "/robots/#{id}"
	expect(page.current_path).to eq(path)
	name_span = find('#botname')
	name_span.assert_text(botname)
end
