require 'rspec/expectations'

Given(/^Bot "([^"]*)" is in the "([^"]*)" weight class$/) do |botname, weight_class|
	# have to populate the weight classes first
	WeightClass.new(name: "Featherweight", pounds: 30).save!
	WeightClass.new(name: "Sportsman", pounds: 30).save!
	WeightClass.new(name: "Hobbyweight", pounds: 12).save!
	
	#WeightClass.all.each { |wc| puts wc.name }
	wc = WeightClass.find_by(name: weight_class)
	#puts wc.name
	mybot = Robot.new( name: botname, weight_class: wc, weapon: 'lifter')
	mybot.save!
end

Given(/^Alice is on the Edit page for "([^"]*)"$/) do |botname|
	mybot = Robot.find_by(name: botname)
	visit "/robots/#{mybot.id}/edit"
end

When(/^Alice changes "([^"]*)"'s weight class to "([^"]*)"$/) do |botname, weight_class_name|
	select(weight_class_name, :from => 'robot[weight_class_id]')
	click_on('Update Robot')
end

Then(/^Alice is directed to the Show page for "([^"]*)"$/) do |botname|
	id = Robot.find_by(name: botname).id
	path = "/robots/#{id}"
	expect(page.current_path).to eq(path)
end

Then(/^"([^"]*)" is now listed as the "([^"]*)" weight class$/) do |botname, weight_class_name|
	bot = Robot.find_by(name: botname)
	weight_class = WeightClass.find_by(name: weight_class_name)
	
	# note: need to surround weight class and name results on show page
	# with a span to identify them
	name_span = find('#botname')
	name_span.assert_text(botname)
	
	wc_span = find('#weight_class')
	wc_span.assert_text(weight_class_name)
end
